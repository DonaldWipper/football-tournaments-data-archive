import requests
import json
import pandas as pd

import url_fifa


class FifaApi():
    def __init__(self):
        pass
    
    @staticmethod
    def get_competitions():
        resp = requests.get(url_fifa.url_competitions)
        return resp.json()['Results']
    
    
    @staticmethod
    def get_matches(id_competition:int, id_season:int):
        resp = requests.get(url_fifa.url_matches.format(idCompetition=id_competition, idSeason=id_season))
        # print(resp.json()['Results'])
        # df = pd.DataFrame(data=)
        return resp.json()['Results']

    @staticmethod
    def get_stadiums(count: int, page: int):
        resp = requests.get(url_fifa.url_stadiums)
        # print(resp.json()['Results'])
        return resp.json()['Results']
    
    
    @staticmethod
    def get_seasons(id_competition:int):
        resp = requests.get(url_fifa.url_seasons.format(idCompetition=id_competition))
        # print(resp.json()['Results'])
        df = pd.DataFrame(data=resp.json()['Results'])
        return  resp.json()['Results']
    
    
    @staticmethod
    def get_teams(offset: int, count_: int = 500):
        print(url_fifa.url_teams.format(page=offset, count=count_))
        resp = requests.get(url_fifa.url_teams.format(page=offset, count=count_))
        return resp.json()['Results']
    
#     @staticmethod
#     def get_timeline(id_competition: int, id_season: int):
#     res = DBConnection.execute(f"""select IdStage, IdMatch
#                                    from matches_fifa_all_time
#                                    where IdSeason = {id_season}""")
#     x = [dict(r) for r in res]
#     columns_json = ["Qualifiers",
#                     "TypeLocalized",
#                     "VarNotificationData",
#                     "EventDescription"]

#     DBConnection.execute(f"drop table if exists timeline_fifa_tmp_history")

#     DBConnection.execute("""
#     create table timeline_fifa_tmp_history
#     as select *
#        from timeline_fifa
#        where  1 = 2""")
# url_timeline = 'https://api.fifa.com/api/v3/timelines/{idCompetition}/{IdSeason}/{IdStage}/{idMatch}?language=en'
#     for match in x:
#         print(match)
#         url = f"https://api.fifa.com/api/v3/timelines/{id_competition}/{id_season}/{match['IdStage']}/{match['IdMatch']}?language=en"
#         s = requests.get(url).json()
#         df1 = pd.DataFrame(data=s['Event'])
#         df1['IdStage'] = match['IdStage']
#         df1['IdMatch'] = match['IdMatch']
#         df1['IdCompetition'] = id_competition
#         df1['IdSeason'] = id_season

#         for c in columns_json:
#             if c in list(df1.columns):
#                 df1[c] = df1.apply(lambda x: json.dumps(x[c]), axis=1)

#         columns = [d for d in list(df1.columns) if d != 'VarNotificationData']
#         df1[columns].to_sql(
#             name="timeline_fifa_tmp_history", con=DBConnection.get_engine(), if_exists="append", index=False
#         )
#         DBConnection.execute(
#             f"""insert into {table_name}
#                 select * from timeline_fifa_tmp_history s
#                 where  s.EventId not in (select EventId from timeline_fifa_history)
#                 """)
        