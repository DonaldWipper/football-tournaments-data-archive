# список голов
insert into goals (id, match_id, player_id, text)
select EventId                                                               as id,
       x.match_id,
       IdPlayer,
       replace(replace(replace(CONCAT(MatchMinute, '',
                                      replace(replace(json_extract(
                                                              json_extract(EventDescription, '$[0]'),
                                                              '$.Description'), '"',
                                                      ''), 'scores!!', '')), '\'', '` '),
                       'successfully converts the penalty', 'pen'), '!', '') as text
from timeline_fifa
         join link_match_to_fifa x on match_id_fifa = IdMatch
where lower(TypeLocalized) like '%goal%'
  and lower(TypeLocalized) not like '%attempt%'



set @row_number = (select max(id) from goals)
insert into goals (id, match_id, player_id, munute, second, text)
select distinct
    (@row_number := @row_number +  1) AS row_number,
       m.id as match_id,
       playerId as player_id,
       minute,
       second,
       concat(minute, '` ', playerName,  ' (', ifnull(playerCountryName, ''), ')')
from uefa_goals
    join link_match_to_uefa on link_match_to_uefa.match_id_uefa = uefa_goals.id
    join matches m on link_match_to_uefa.match_id = m.id
where m.competition_id = 1476
