# вставим новый чемпионат
insert into matches (id,
                     competition_id,
                     place_id,
                     stage_id,
                     status_id,
                     date,
                     local_date,
                     home_team_id,
                     away_team_id,
                     goals_home_team,
                     goals_away_team,
                     total_home_goals,
                     total_away_goals,
                     penalty_shootout_home_goals,
                     penalty_shootout_away_goals,
                     game_order)
select uu.id,
       c.id                as competition_id,
       p.id                as place_id,
       s.id                as stage_id,
       match_status.id     as status_id,
       uu.dateTime         as date,
       DATE_FORMAT(
               DATE_ADD(
                       STR_TO_DATE(uu.dateTime, '%Y-%m-%dT%H:%i:%sZ'),
                       INTERVAL utcOffsetInHours HOUR
                   ),
               '%Y-%m-%dT%H:%i:%sZ'
           )               as local_date,
       t_h.id              as home_team_id,
       t_a.id              as away_team_id,
       uu.scoreRegularHome as goals_home_team,
       uu.scoreRegularAway as goals_away_team,
       uu.scoreTotalHome   as total_home_goals,
       uu.scoreTotalAway   as total_away_goals,
       uu.scorePenaltyHome as penalty_shootout_home_goals,
       uu.scorePenaltyAway as penalty_shootout_away_goals,
       uu.matchNumber      as game_order


from (select case
                 when groupName is null then roundName
                 else groupName
                 end as stage_name,
             u.*
      from uefa_matches u
      where u.seasonYear = 2020) uu
         join stages s
              on uu.stage_name = s.title
         join competitions c
              on c.year = uu.seasonYear
         join teams t_h
              on t_h.short_name = uu.homeTeamcountryCode
         join teams t_a
              on t_a.short_name = uu.awayTeamcountryCode

         left join link_stadium_to_uefa lnk_stad
                   on lnk_stad.stadium_id_uefa = uu.stadiumId
         left join places p on lnk_stad.place_id = p.id

         left join match_status on match_status.status = lower(uu.status)

order by uu.id;
