from requests import get as requests_get
# from flaresolver import get as requests_get
import json
import pandas as pd

from url_uefa import URL_MATCHES, URL_COMPETITIONS, URL_TEAMS, URL_STADIUMS

# window.environment = 'prd';
#     window.uefaBaseUrl = '//www.uefa.com';
#     window.vsmBaseUrl = '';
#     window.uefaApiBaseUrl = '/api/v1/';
#     window.liveBlogBasePath = '/api/v2/blogs/';

#     window.competitionId = '3';
#     window.competitionFolder = 'uefaeuro';
#     window.competitionBanner = 'uefaeuro';
#     window.competitionTracking = 'euro';
#     window.competitionCode = 'euro2024';
#     window.competitionName = 'uefaeuro';
#     window.competitionUrl = 'uefaeuro';
#     window.isClub = false;
#     window.currentSeason = 2024;
#     window.imgBaseUrl = 'https://img.uefa.com';


# window.matchApiUrl = 'https://match.uefa.com/';
# window.compApiUrl = 'https://comp.uefa.com/';
# window.compStatsApiUrl = 'https://compstats.uefa.com/';
# indow.drawApiUrl = 'https://fsp-draw-service.uefa.com/';
# window.matchStatsApiUrl = 'https://matchstats.uefa.com/';
# window.masApiUrl = 'https://mas.uefa.com/';
# window.domesticApiUrl = 'https://domestic.uefa.com/';
# window.cardApiUrl = 'https://fsp-data-cards-service.uefa.com/';
# window.performanceApiBaseUrl = 'https://fsp-players-ranking-service.uefa.com/';
# window.cobaltApiUrl = 'https://editorial.uefa.com/api/';
# window.cobaltApiKey = 'bc1ff15c-814f-4318-b374-50ad9c1b7294';
# window.cobaltBaseUrl = 'https://editorial.uefa.com/';
# window.cobaltImgBaseUrl = 'https://editorial.uefa.com/';
# window.sponsorApiUrl = 'https://fsp-sponsor-service.uefa.com/';

HEADERS = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36",
    "Content-Type": "application/json"
}


class UefaApi:
    def __init__(self):
        pass

    @staticmethod
    def get_matches(competition_id: int, season_year: int):
        print(URL_MATCHES.format(
            competition_id=competition_id,
            season_year=season_year,
            timeout=30,
        ))
        resp = requests_get(
            URL_MATCHES.format(
                competition_id=competition_id,
                season_year=season_year,
                timeout=30,
            ),
            headers=HEADERS,
        )
        return resp.json()

    @staticmethod
    def get_competitions():
        resp = requests_get(URL_COMPETITIONS, headers=HEADERS)
        return resp.json()

    @staticmethod
    def get_stadiums(offset: int = 1):
        resp = requests_get(
            URL_STADIUMS.format(offset=offset), headers=HEADERS, timeout=30
        )
        return resp.json()

    @staticmethod
    def get_teams(offset: int, competition_id: int):
        resp = requests_get(
            URL_TEAMS.format(
                offset=offset,
                competition_id=competition_id
            ),
            timeout=30,
            headers=HEADERS
        )
        return resp.json()
