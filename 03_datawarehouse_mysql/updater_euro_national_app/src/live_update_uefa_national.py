import json
import time
import pandas as pd
from sqlalchemy import text

from sql import DBConnection
from uefa_api import UefaApi

YEAR = 2024
COMPETITION_ID = 3


def update_sql_matches():
    SQL_MATCHES = f"""
    UPDATE matches m
    JOIN link_match_to_uefa s2 ON m.id = s2.match_id
    JOIN uefa_matches_{YEAR}_staging s ON s.id = s2.match_id_uefa
    LEFT JOIN teams ltct_home ON ltct_home.short_name = s.homeTeamcountryCode
    LEFT JOIN teams ltct_away ON ltct_away.short_name = s.awayTeamcountryCode
    LEFT JOIN match_status ms ON ms.status = LOWER(s.status)
    SET
    
        m.status_id = ifnull(ms.id, 4),
        m.home_team_id = ltct_home.id,
        m.away_team_id = ltct_away.id,
        m.goals_home_team = s.scoreRegularHome,
        m.goals_away_team = s.scoreRegularAway,
        m.total_home_goals = s.scoreTotalHome,
        m.total_away_goals = s.scoreTotalAway,
        m.penalty_shootout_home_goals = s.scorePenaltyHome,
        m.penalty_shootout_away_goals = s.scorePenaltyAway

    WHERE
        COALESCE(m.home_team_id, -1) != COALESCE(ltct_home.id, -1) OR
        COALESCE(m.away_team_id , -1) != COALESCE(ltct_away.id, -1) OR
        COALESCE(m.total_home_goals, -1) != COALESCE(s.scoreTotalHome, -1) OR
        COALESCE(m.total_away_goals, -1) != COALESCE(s.scoreTotalAway, -1) OR
        COALESCE(m.penalty_shootout_home_goals, -1) != COALESCE(s.scorePenaltyHome, -1) OR
        COALESCE(m.penalty_shootout_away_goals, -1) != COALESCE(s.scorePenaltyAway, -1) OR
        COALESCE(m.status_id, -1) != COALESCE(ifnull(ms.id, 4), -1);
    """
    engine = DBConnection.get_engine()
    # print(SQL_MATCHES)
    with engine.connect() as conn:
        conn.execute(text(SQL_MATCHES))
        conn.commit()


def update_sql_goals():
    CLEAN_UP_GOALS = f"""delete goals
                    from goals
                    join link_match_to_uefa on goals.match_id = link_match_to_uefa.match_id
                    left join uefa_goals_2024_staging s2
                    on s2.player_id = goals.player_id
                    and s2.minute = goals.minute
                    and s2.second = goals.second
                    where link_match_to_uefa.match_id_uefa in (select match_id
                    from uefa_goals_{YEAR}_staging s)
                    and s2.player_id is null"""

    SQL_GOALS = f"""insert into goals (id, match_id, player_id, minute, second, text)
    select (@row_number := @row_number + 1) AS id,
            lnk.match_id,
            s.player_id,
            s.minute,
            s.second,
            s.text
    from uefa_goals_2024_staging s
    join link_match_to_uefa lnk
    on s.match_id = lnk.match_id_uefa
    left join goals on goals.match_id = lnk.match_id
    and goals.minute = s.minute
    and goals.player_id = s.player_id
    where goals.match_id is null;
    """
    engine = DBConnection.get_engine()

    # print(SQL_GOALS)
    with engine.connect() as conn:
        conn.execute(text(CLEAN_UP_GOALS))
        conn.execute(text('set @row_number = (select max(id) from  goals);'))
        conn.execute(text(SQL_GOALS))
        conn.commit()


def generate_text(text: str, goal_type: str | None = None):
    if goal_type is None:
        return text

    if goal_type.lower() == 'penalty':
        return f"{text} pen"
    if goal_type.lower() == 'own':
        return f"{text} own"

    return f"{text}"


def ingest_goals(match_id, score_events):
    data = []
    #  + score_events.get('penaltyScorers', [])
    for event in score_events.get('scorers', []):
        player = event['player']
        player_id = player['id']
        player_name = player['translations']['name']['EN']
        country_code = player['countryCode']
        minute = event['time']['minute']
        second = event['time'].get('second', 0)
        goal_type = event.get('goalType', '')

        text = f"{minute}` {player_name}({country_code})"
        text = generate_text(text, goal_type)

        data.append({
            "id": event['id'],
            "match_id": match_id,
            "player": player_name,
            "player_id": player_id,
            "countryCode": country_code,
            "minute": minute,
            "second": second,
            "text": text
        })
    return data


def ingest_matches(years):
    ress = []
    for year in years:
        json_data = UefaApi.get_matches(competition_id=COMPETITION_ID, season_year=year)
        # print(year)
        if len(json_data) > 0:
            ress += json_data

    data_goals = []
    for match in ress:
        match['matchday_json'] = json.dumps(match.get('matchday', {}))
        match['scorers_json'] = json.dumps(match.get('scorers', {}))
        events = match.get('playerEvents', {})
        if events:
            data_goals += ingest_goals(match.get('id'), events)
        match['playerEvents_json'] = json.dumps(events)

    df_goals = pd.DataFrame(data_goals)
    df = pd.json_normalize(ress)
    #
    # for i in sorted(list(df.columns)):
    #     print(i)

    df['AwayCountryName'] = df['awayTeam.translations.countryName.EN']
    df['HomeCountryName'] = df['homeTeam.translations.countryName.EN']
    df['awayTeamcountryCode'] = df['awayTeam.countryCode']
    df['homeTeamcountryCode'] = df['homeTeam.countryCode']
    df['awayTeamId'] = df['awayTeam.id']
    df['homeTeamId'] = df['homeTeam.id']
    df['utcOffsetInHours'] = df['kickOffTime.utcOffsetInHours']
    df['dateTime'] = df['kickOffTime.dateTime']
    df['UnixTimestamp'] = pd.to_datetime(df['dateTime']).astype(int) / 10 ** 9

    score_columns = [
        'score.aggregate.away', 'score.aggregate.home', 'score.penalty.away', 'score.penalty.home',
        'score.regular.away', 'score.regular.home', 'score.total.away', 'score.total.home'
    ]
    for col in score_columns:
        if col in df.columns:
            df[col.replace('score.', 'score').replace('.', '_')] = df[col]
        else:
            df[col.replace('score.', 'score').replace('.', '_')] = None

    df['stadiumCity'] = df['stadium.city.translations.name.EN']
    df['stadiumCapacity'] = df['stadium.capacity']
    df['stadiumLatitude'] = df['stadium.geolocation.latitude']
    df['stadiumLongitude'] = df['stadium.geolocation.longitude']
    df['stadiumId'] = df['stadium.id']
    df['stadiumName'] = df['stadium.translations.name.EN']
    df['competitionId'] = df['competition.id']
    df['groupName'] = df['group.metaData.groupName']
    df['roundName'] = df['round.metaData.name']
    df['scorers'] = df['playerEvents.scorers']

    def map_or_fill(df, new_col, source_col):
        from numpy import nan
        df[new_col] = df[source_col] if source_col in df.columns else nan

    # Applying the function to map columns or fill with NaN
    map_or_fill(df, 'scoreAggregateAway', 'score.aggregate.away')
    map_or_fill(df, 'scoreAggregateHome', 'score.aggregate.home')
    map_or_fill(df, 'scorePenaltyAway', 'score.penalty.away')
    map_or_fill(df, 'scorePenaltyHome', 'score.penalty.home')
    map_or_fill(df, 'scoreRegularAway', 'score.regular.away')
    map_or_fill(df, 'scoreRegularHome', 'score.regular.home')
    map_or_fill(df, 'scoreTotalAway', 'score.total.away')
    map_or_fill(df, 'scoreTotalHome', 'score.total.home')

    if 'matchAttendance' not in df.columns:
        df['matchAttendance'] = None

    print(list(df.columns))
    selected_columns = [
        'id', 'awayTeamcountryCode', 'matchday.seasonYear', 'AwayCountryName', 'HomeCountryName', 'homeTeamcountryCode',
        'dateTime', 'UnixTimestamp', 'utcOffsetInHours', 'matchAttendance', 'matchNumber', 'status', 'matchday_json',
        'scoreAggregateAway', 'scoreAggregateHome', 'scorePenaltyAway', 'scorePenaltyHome', 'scoreRegularAway',
        'scoreRegularHome', 'scoreTotalAway', 'scoreTotalHome', 'type', 'stadiumCity', 'stadiumCapacity',
        'stadiumLatitude', 'stadiumLongitude', 'stadiumId', 'stadiumName', 'competitionId', 'groupName', 'roundName', 'scorers_json',
        'playerEvents_json'
    ]

    df_selected = df[selected_columns]

    for c in ['matchAttendance', 'matchNumber', 'utcOffsetInHours', 'stadiumCapacity']:
        df_selected[c] = df_selected[c].fillna(0).astype(int)

    # print(df_selected)
    engine = DBConnection.get_engine()
    df_selected.to_sql(name=f"uefa_matches_{YEAR}_staging", con=engine, if_exists='replace', index=False)
    df_goals.to_sql(name=f"uefa_goals_{YEAR}_staging", con=engine, if_exists='replace', index=False)


if __name__ == '__main__':
    while True:
        print('run updater')
        ingest_matches([YEAR])
        update_sql_matches()
        update_sql_goals()
        time.sleep(30)
