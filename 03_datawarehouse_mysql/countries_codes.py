import requests
from bs4 import BeautifulSoup

url = "https://www.worlddata.info/countrycodes.php"
response = requests.get(url)

if response.status_code == 200:
    soup = BeautifulSoup(response.text, 'html.parser')
    table = soup.find('table')

    country_data = []
    headers = []
    for row in table.find_all('tr'):

        for th in row.find_all('th'):
            br_tag = th.find('br')
            if br_tag:
                br_tag.replace_with(' ')
            headers.append(th.text.strip())

        columns = row.find_all('td')
        values = [c.text.strip() for c in columns]
        if len(values) > 0:
            country_data.append(dict(zip(headers, values)))
        # if len(columns) > 4:
        #
        #     country_name = columns[0].text.strip()
        #     alpha2 = columns[1].text.strip()
        #     alpha3 = columns[2].text.strip()

    # Print the country data
    print(country_data)
else:
    print("Error:", response.status_code)
