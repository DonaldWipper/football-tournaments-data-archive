--uefa_matches_2024_staging стейджовая таблица с матчами


--  проверить отсутствующие стадионы, eсли отсутствуют, нужно собирать из
select *
from uefa_matches_2024_staging
where stadiumId  not in (select id from uefa_stadiums)

-- вставляем новые стадионы
set @row_number = (select max(id) from  places)
insert into places (id,
                    stadium,
                    city,
                    short_name,
                    capacity,
                    lat,
                    lng)

select distinct (@row_number := @row_number + 1) AS id,
                s.name,
                s.cityName,
                lower(Name),
                Capacity,
                Latitude  as latitude,
                Longitude as longitude

from uefa_stadiums s
         left join (select lng, lat, min(id) as id
                    from places
                    group by lng, lat) p
                   on s.Latitude = p.lat
                       and s.Longitude = p.lng

where s.id not in (select stadium_id_uefa from link_stadium_to_uefa)
  and p.lat is null




--  проверить отсутствующие команды-сборные, eсли отсутствуют, нужно собирать из
select *
from uefa_matches_2024_staging
where homeTeamcountryCode not in (select short_name from teams)
   or awayTeamcountryCode not in (select short_name from teams)

-- есть нужный турнир в таблице, если нет нужно добавлять
select *
from uefa_competitions
where id = 3


--  есть нужные матчи в таблицах
select *
from uefa_matches_2024_staging
where seasonYear = 2024
  and compettionId = 3


--  добавим новые команду в таблицу
set @row_number = (select max(id) from  teams)
insert into teams
select (@row_number := @row_number + 1) AS id,
       countryCode,
       c.`Alpha-2 code`,
       internationalName
from uefa_national_teams nt
         join countries_alpha_codes c
              on c.`Alpha-3 code` = nt.countryCode
         left join teams t
                   on t.short_name  = nt.countryCode
where t.id is null


--вставляем новый чемпионат
set @row_number = (select max(id) from  competitions)
insert into  competitions (id,
                           number_of_match_days,
                           number_of_games,
                           number_of_teams,
                           last_updated,
                           caption,
                           abbreviation,
                           league,
                           year,
                           season,
                           team_category)

select  (@row_number := @row_number + 1) AS id,
        x.number_of_match_days,
        x.number_of_games,
        x. number_of_teams,
        now() as last_updated,
        'UEFA European Championship 2024 Germany' as caption,
        concat(code, ' ', seasonYear) as abbreviation,
        'EC' as league,
        seasonYear as year,
        seasonYear as season,
        s.teamCategory
from uefa_competitions s
         cross join (select seasonYear,
                            count(distinct awayTeamcountryCode)                                   as number_of_teams,
                            count(*)                                                              as number_of_games,
                            count(distinct
                                  DATE_FORMAT(FROM_UNIXTIME(UnixTimestamp), '%Y-%m-%d'))          as number_of_match_days
                     from uefa_matches_2024_staging u
                     where seasonYear = 2024
                     group by seasonYear) as x
where s.id = 3




set @row_number = (select max(id) from  matches)
insert into matches(id, competition_id, place_id, stage_id, status_id, date, local_date, home_team_id, away_team_id, goals_home_team, goals_away_team, total_home_goals, total_away_goals, penalty_shootout_home_goals, penalty_shootout_away_goals, game_order)
select (@row_number := @row_number + 1) AS id,
       c.id                             as competition_id,
       p.id                             as place_id,
       s.id                             as stage_id,
       match_status.id                  as status_id,
       uu.dateTime                      as date,
       DATE_FORMAT(
               DATE_ADD(
                       STR_TO_DATE(uu.dateTime, '%Y-%m-%dT%H:%i:%sZ'),
                       INTERVAL utcOffsetInHours HOUR
               ),
               '%Y-%m-%dT%H:%i:%sZ'
       )                            as local_date,
       ltct_home.id          as home_team_id,
       ltct_away.id           as away_team_id,
       uu.scoreRegularHome              as goals_home_team,
       uu.scoreRegularAway              as goals_away_team,
       uu.scoreTotalHome                as total_home_goals,
       uu.scoreTotalAway                as total_away_goals,
       uu.scorePenaltyHome as penalty_shootout_home_goals,
       uu.scorePenaltyAway as penalty_shootout_away_goals,
       uu.matchNumber      as game_order

from (select case
                 when groupName is null then roundName
                 else groupName
                 end as stage_name,
             u.*
      from uefa_matches_2024_staging u
      where u.seasonYear = 2024) uu
         join stages s
              on uu.stage_name = s.title
         join competitions c
              on c.year = uu.seasonYear
                  and c.league = 'EC'

         left join teams ltct_home
                   on ltct_home.short_name = uu.homeTeamcountryCode

         left join teams ltct_away
                   on ltct_away.short_name = uu.awayTeamcountryCode


         left join link_stadium_to_uefa lnk_stad
                   on lnk_stad.stadium_id_uefa = uu.stadiumId
         left join places p on lnk_stad.place_id = p.id

         left join match_status on match_status.status = lower(uu.status)

order by uu.id;


-- делаем связку с основной табли
insert into link_match_to_uefa (match_id, match_id_uefa)
select m.id, s.id
from matches m
         join uefa_matches_2024_staging s
              on m.game_order = s.matchNumber
where m.competition_id = 1480
