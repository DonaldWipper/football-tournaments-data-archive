-- First, update the main matches table with the changed fields from the staging table
UPDATE matches m
    JOIN link_match_to_uefa s2 ON m.id = s2.match_id
    JOIN uefa_matches_2024_staging s ON s.id = s2.match_id_uefa
#     LEFT JOIN stages stg ON (CASE WHEN s.groupName IS NULL THEN s.roundName ELSE s.groupName END) = stg.title
#     LEFT JOIN competitions c ON c.year = s.seasonYear AND c.league = 'EC'
    LEFT JOIN teams ltct_home ON ltct_home.short_name = s.homeTeamcountryCode
    LEFT JOIN teams ltct_away ON ltct_away.short_name = s.awayTeamcountryCode
#     LEFT JOIN link_stadium_to_uefa lnk_stad ON lnk_stad.stadium_id_uefa = s.stadiumId
#     LEFT JOIN places p ON lnk_stad.place_id = p.id
    LEFT JOIN match_status ms ON ms.status = LOWER(s.status)
SET
    #     m.competition_id = c.id,
    #     m.place_id = p.id,
    #     m.stage_id = stg.id,
    m.status_id = ms.id,
    #     m.date = s.dateTime,
    #     m.local_date = DATE_FORMAT(DATE_ADD(STR_TO_DATE(s.dateTime, '%Y-%m-%dT%H:%i:%sZ'), INTERVAL s.utcOffsetInHours HOUR), '%Y-%m-%dT%H:%i:%sZ'),
    m.home_team_id = ltct_home.id,
    m.away_team_id = ltct_away.id,
    m.goals_home_team = s.scoreRegularHome,
    m.goals_away_team = s.scoreRegularAway,
    m.total_home_goals = s.scoreTotalHome,
    m.total_away_goals = s.scoreTotalAway,
    m.penalty_shootout_home_goals = s.scorePenaltyHome,
    m.penalty_shootout_away_goals = s.scorePenaltyAway
    #     m.game_order = s.matchNumber
WHERE
    COALESCE(m.goals_home_team, -1) != COALESCE(s.scoreRegularHome, -1) OR
    COALESCE(m.goals_away_team, -1) != COALESCE(s.scoreRegularAway, -1) OR
    COALESCE(m.total_home_goals, -1) != COALESCE(s.scoreTotalHome, -1) OR
    COALESCE(m.total_away_goals, -1) != COALESCE(s.scoreTotalAway, -1) OR
    COALESCE(m.penalty_shootout_home_goals, -1) != COALESCE(s.scorePenaltyHome, -1) OR
    COALESCE(m.penalty_shootout_away_goals, -1) != COALESCE(s.scorePenaltyAway, -1);



UPDATE matches m
    JOIN link_match_to_uefa s2 ON m.id = s2.match_id
    JOIN uefa_matches_2024_staging s ON s.id = s2.match_id_uefa
    LEFT JOIN teams ltct_home ON ltct_home.short_name = s.homeTeamcountryCode
    LEFT JOIN teams ltct_away ON ltct_away.short_name = s.awayTeamcountryCode
    LEFT JOIN match_status ms ON ms.status = LOWER(s.status)
SET

    m.status_id = ms.id,
    m.home_team_id = ltct_home.id,
    m.away_team_id = ltct_away.id,
    m.goals_home_team = s.scoreRegularHome,
    m.goals_away_team = s.scoreRegularAway,
    m.total_home_goals = s.scoreTotalHome,
    m.total_away_goals = s.scoreTotalAway,
    m.penalty_shootout_home_goals = s.scorePenaltyHome,
    m.penalty_shootout_away_goals = s.scorePenaltyAway

WHERE
    COALESCE(m.goals_home_team, -1) != COALESCE(s.scoreRegularHome, -1) OR
    COALESCE(m.goals_away_team, -1) != COALESCE(s.scoreRegularAway, -1) OR
    COALESCE(m.total_home_goals, -1) != COALESCE(s.scoreTotalHome, -1) OR
    COALESCE(m.total_away_goals, -1) != COALESCE(s.scoreTotalAway, -1) OR
    COALESCE(m.penalty_shootout_home_goals, -1) != COALESCE(s.scorePenaltyHome, -1) OR
    COALESCE(m.penalty_shootout_away_goals, -1) != COALESCE(s.scorePenaltyAway, -1);



set @row_number = (select max(id) from  goals)
insert into goals (id, match_id, player_id, minute, second, text)
select (@row_number := @row_number + 1) AS id,
       lnk.match_id,
       s.player_id,
       s.minute,
       s.second,
       s.text
from uefa_goals_2024_staging s
         join link_match_to_uefa lnk
              on s.match_id = lnk.match_id_uefa
         left join goals on goals.match_id = lnk.match_id
    and goals.minute = s.minute
    and goals.second = s.second
where goals.match_id is null