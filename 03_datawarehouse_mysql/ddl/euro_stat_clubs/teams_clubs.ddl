create table teams_clubs
(
    id                  bigint null,
    name                text   null,
    country_name        text   null,
    team_short_name     text   null,
    country_short_name  text   null, #  ISO 3166-1 alpha-3
    country_short_name2 text   null, #  ISO 3166-1 alpha-2
    constraint id
        unique (id)
);
