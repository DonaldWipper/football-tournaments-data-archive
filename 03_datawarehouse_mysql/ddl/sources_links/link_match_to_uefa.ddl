
create table sources_links.link_match_to_uefa
(
    match_id      bigint not null,
    match_id_uefa bigint not null
);


insert into link_match_to_uefa (match_id, match_id_uefa)
select m.id, s.id
from matches m
         join uefa_matches_2024_staging s
              on m.game_order = s.matchNumber
where m.competition_id = 1480