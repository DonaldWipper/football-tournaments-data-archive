create table link_competitions_to_fifa
(
    competition_id      bigint not null,
    season_id_fifa      bigint not null,
    competition_id_fifa bigint not null
);