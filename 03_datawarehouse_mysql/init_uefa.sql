# обновить дату в таблице для старых данных
create table tmp_date_update as
select matches.id,
       um.dateTime,
       um.utcOffsetInHours,
       DATE_FORMAT(
               DATE_ADD(
                       STR_TO_DATE(um.dateTime, '%Y-%m-%dT%H:%i:%sZ'),
                       INTERVAL 2 HOUR
                   ),
               '%Y-%m-%dT%H:%i:%sZ'
           ) as date_local

from matches
         left join link_match_to_uefa lmtu on matches.id = lmtu.match_id
         left join uefa_matches um on lmtu.match_id_uefa = um.id
where date like '%+%'
  and um.dateTime is not null


# cписок стадионов с дупликатами
select *
from (select lat, lng
      from places
      group by lat, lng
      having count(*) > 1) x
         join places p
              on x.lng = p.lng
                  and x.lat = p.lat
order by stadium

# cвязка мест со стадионами по координатам
create view link_stadium_to_uefa as
select p.id as place_id,
       s.id as stadium_id_uefa
from uefa_stadiums s
         join (select lng, lat, min(id) as id
               from places
               group by lng, lat) p
              on s.latitude = p.lat
                  and s.longitude = p.lng




insert into link_stadium_to_uefa (place_id, stadium_id_uefa)
values (364, 74459);

#  вставка несуществующего стадиона
insert into places
select 364                                         as id,
       uefa_stadiums.name                          as stadium,
       cityName                                    as city,
       lower(replace(uefa_stadiums.name, ' ', '')) as short_name,
       capacity,
       1476                                        as competition_id,
       latitude,
       longitude
from uefa_stadiums
where id = 74459;


# вставим новые матчи чемпионата
insert into matches (id,
                     competition_id,
                     place_id,
                     stage_id,
                     status_id,
                     date,
                     local_date,
                     home_team_id,
                     away_team_id,
                     goals_home_team,
                     goals_away_team,
                     total_home_goals,
                     total_away_goals,
                     penalty_shootout_home_goals,
                     penalty_shootout_away_goals,
                     game_order)
select uu.id,
       c.id                as competition_id,
       p.id                as place_id,
       s.id                as stage_id,
       match_status.id     as status_id,
       uu.dateTime         as date,
       DATE_FORMAT(
               DATE_ADD(
                       STR_TO_DATE(uu.dateTime, '%Y-%m-%dT%H:%i:%sZ'),
                       INTERVAL utcOffsetInHours HOUR
                   ),
               '%Y-%m-%dT%H:%i:%sZ'
           )               as local_date,
       t_h.id              as home_team_id,
       t_a.id              as away_team_id,
       uu.scoreRegularHome as goals_home_team,
       uu.scoreRegularAway as goals_away_team,
       uu.scoreTotalHome   as total_home_goals,
       uu.scoreTotalAway   as total_away_goals,
       uu.scorePenaltyHome as penalty_shootout_home_goals,
       uu.scorePenaltyAway as penalty_shootout_away_goals,
       uu.matchNumber      as game_order


from (select case
                 when groupName is null then roundName
                 else groupName
                 end as stage_name,
             u.*
      from uefa_matches u
      where u.seasonYear = 2020) uu
         join stages s
              on uu.stage_name = s.title
         join competitions c
              on c.year = uu.seasonYear
         join teams t_h
              on t_h.short_name = uu.homeTeamcountryCode
         join teams t_a
              on t_a.short_name = uu.awayTeamcountryCode

         left join link_stadium_to_uefa lnk_stad
                   on lnk_stad.stadium_id_uefa = uu.stadiumId
         left join places p on lnk_stad.place_id = p.id

         left join match_status on match_status.status = lower(uu.status)

order by uu.id;


# вставка новые места
set @row_number = (select max(id)
                   from places);


insert into places(id, stadium, city, short_name, lat, lng)
select (@row_number := @row_number + 1) AS id,
       stadium,
       city,
       short_name,
       lat,
       lng
from (select distinct s.name                          as stadium,
                      s.cityName                      as city,
                      lower(replace(s.name, ' ', '')) as short_name,
                      s.capacity,
                      s.latitude                      as lat,
                      s.longitude                     as lng

      from uefa_matches_clubs u
               join uefa_stadiums s
                    on u.stadiumId = s.id
               left join (select lng, lat, min(id) as id
                          from places
                          group by lng, lat) p
                         on u.stadiumLatitude = p.lat
                             and u.stadiumLongitude = p.lng

               left join link_stadium_to_uefa lstu
                         on lstu.stadium_id_uefa = u.stadiumId

      where lstu.stadium_id_uefa is null
        and p.lat is null) as x;



# вставка нового турнира лиги
set @row_number = (select max(id) from  competitions)
insert into  competitions (id,
                           number_of_match_days,
                           number_of_games,
                           number_of_teams,
                           last_updated,
                           caption,
                           abbreviation,
                           league,
                           year,
                           season)

select  (@row_number := @row_number + 1) AS id,
        x.number_of_match_days,
        x.number_of_games,
        x. number_of_teams,
        now() as last_updated,
        concat(name, ' ', seasonYear) as caption,
        concat(code, ' ', seasonYear) as abbreviation,
        code as league,
        seasonYear as year,
        concat(seasonYear-1,'/',seasonYear) as season
from uefa_competitions s
         cross join (select seasonYear,
                            count(distinct awayTeamId)                                            as number_of_teams,
                            count(*)                                                              as number_of_games,
                            count(distinct
                                  DATE_FORMAT(FROM_UNIXTIME(UnixTimestamp), '%Y-%m-%d'))          as number_of_match_days
                     from uefa_matches_clubs u
                     where seasonYear = 2023
                     group by seasonYear) as x

where s.id = 1




# вставка клуюов
set @row_number = ifnull((select max(id) from  matches_clubs), 0);

insert into matches_clubs (id,
                           competition_id,
                           place_id,
                           stage_id,
                           status_id,
                           date,
                           local_date,
                           home_team_id,
                           away_team_id,
                           goals_home_team,
                           goals_away_team,
                           total_home_goals,
                           total_away_goals)

select (@row_number := @row_number + 1) AS id,
       c.id                             as competition_id,
       p.id                             as place_id,
       s.id                             as stage_id,
       match_status.id                  as status_id,
       uu.dateTime                      as date,
       DATE_FORMAT(
               DATE_ADD(
                       STR_TO_DATE(uu.dateTime, '%Y-%m-%dT%H:%i:%sZ'),
                       INTERVAL utcOffsetInHours HOUR
                   ),
               '%Y-%m-%dT%H:%i:%sZ'
           )                            as local_date,
       ltct_home.team_club_id           as home_team_id,
       ltct_away.team_club_id           as away_team_id,
       uu.scoreRegularHome              as goals_home_team,
       uu.scoreRegularAway              as goals_away_team,
       uu.scoreTotalHome                as total_home_goals,
       uu.scoreTotalAway                as total_away_goals
#        uu.scorePenaltyHome as penalty_shootout_home_goals,
#        uu.scorePenaltyAway as penalty_shootout_away_goals,
#        uu.matchNumber      as game_order

from (select case
                 when groupName is null then roundName
                 else groupName
                 end as stage_name,
             u.*
      from uefa_matches_clubs u
      where u.seasonYear = 2023) uu
         join stages s
              on uu.stage_name = s.title
         join competitions c
              on c.year = uu.seasonYear
                  and c.league = 'UCL'

         join link_teams_clubs_to_uefa ltct_home
              on ltct_home.team_id_uefa = uu.homeTeamId

         join link_teams_clubs_to_uefa ltct_away
              on ltct_away.team_id_uefa = uu.awayTeamId


         left join link_stadium_to_uefa lnk_stad
                   on lnk_stad.stadium_id_uefa = uu.stadiumId
         left join places p on lnk_stad.place_id = p.id

         left join match_status on match_status.status = lower(uu.status)

order by uu.id;

# связка клубных матчей
insert into link_match_clubs_to_uefa (match_clubs_id, match_id_uefa)
select match_clubs_id, match_id_uefa
from (select distinct x.id  as match_clubs_id,
                      x2.id as match_id_uefa
      from (SELECT UNIX_TIMESTAMP(CONVERT_TZ(date, '+00:00', @@session.time_zone)) AS unix_timestamp,
                   id,
                   home_team_id
            from matches_clubs) x

               join link_teams_clubs_to_uefa s
                    on s.team_club_id = x.home_team_id

               join uefa_matches_clubs x2
                    on x.unix_timestamp = x2.UnixTimestamp
                        and x2.homeTeamId = s.team_id_uefa
      where x.id not in (select match_clubs_id from link_match_clubs_to_uefa)
        and x2.id not in (select match_id_uefa from link_match_clubs_to_uefa)) as x3

