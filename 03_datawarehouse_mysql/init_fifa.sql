# список голов
insert into goals (id, match_id, player_id, text)
select EventId                                                               as id,
       x.match_id,
       IdPlayer,
       replace(replace(replace(CONCAT(MatchMinute, '',
                                      replace(replace(json_extract(
                                                              json_extract(EventDescription, '$[0]'),
                                                              '$.Description'), '"',
                                                      ''), 'scores!!', '')), '\'', '` '),
                       'successfully converts the penalty', 'pen'), '!', '') as text
from timeline_fifa
         join link_match_to_fifa x on match_id_fifa = IdMatch
where lower(TypeLocalized) like '%goal%'
  and lower(TypeLocalized) not like '%attempt%'





# добавить новые команды для национального турнира
set @row_number = (select max(id)
                   from teams)

insert into teams (id, short_name, short_name2, name)
select (@row_number := @row_number + 1) AS id,
       ft.Abbreviation                  as short_name,
       cas.`ISO 3166-1 alpha3`          as short_name2,
       ft.Name                          as name
from (select distinct HomeAbbreviation as Abbreviation
      from fifa_matches_u20 uu
               left join teams t_h
                         on t_h.short_name = uu.HomeAbbreviation
      where t_h.short_name is null

      union

      select distinct AwayAbbreviation
      from fifa_matches_u20 uu
               left join teams t_a
                         on t_a.short_name = uu.AwayAbbreviation
      where t_a.short_name is null) as x
         join fifa_teams ft
              on ft.Abbreviation = x.Abbreviation
                  and ft.Gender = 1
                  and ft.AgeType = 7
         join countrycodes cas
              on cas.`ISO 3166-1 alpha3` = ft.Abbreviation


# вставка по другому полю IOC, если не совпадает
set @row_number = (select max(id)
                   from teams)

insert into teams (id, short_name, short_name2, name)

select (@row_number := @row_number + 1) AS id,
       ft.Abbreviation                  as short_name,
       cas.`IOC`                        as short_name2,
       ft.Name                          as name
from (select distinct HomeAbbreviation as Abbreviation
      from fifa_matches_u20 uu
               left join teams t_h
                         on t_h.short_name = uu.HomeAbbreviation
      where t_h.short_name is null

      union

      select distinct AwayAbbreviation
      from fifa_matches_u20 uu
               left join teams t_a
                         on t_a.short_name = uu.AwayAbbreviation
      where t_a.short_name is null) as x
         join fifa_teams ft
              on ft.Abbreviation = x.Abbreviation
                  and ft.Gender = 1
                  and ft.AgeType = 7
         join countrycodes cas
              on cas.`IOC` = ft.Abbreviation


# cвязка игр
insert into link_match_to_fifa
select id,
       IdMatch
from (select uu.IdMatch,
             1478        as competition_id,
             MatchNumber as game_order
      from (select case
                       when StageNameDescription = 'Group Matches' then GroupNameDescription
                       else StageNameDescription
                       end as stage_name,
                   uu.*
            from fifa_matches_u20 uu
            where IdSeason = 284700) as uu
               left join teams t_h
                         on t_h.short_name = uu.HomeAbbreviation
               left join teams t_a
                         on t_a.short_name = uu.AwayAbbreviation
               left join stages s
                         on s.title = uu.stage_name
               left join link_match_status_to_fifa lmstf
                         on lmstf.match_status_id_fifa = uu.MatchStatus
               left join places p on uu.StadiumName = p.stadium) as x
         join matches m
              on m.competition_id = x.competition_id
                  and m.game_order = x.game_order
where id not in (select match_id from link_match_to_fifa)


# табличка связка чемпионатов fifa
insert into link_competitions_to_fifa (competition_id, season_id_fifa, competition_id_fifa)
select c.id,
       fcs.IdSeason,
       fcs.IdCompetition
from fifa_competition_seasons fcs
         join competitions c
              on fcs.Abbreviation = c.abbreviation
where IdSeason = 284700



create view matches_u20_staging as
select lctf.competition_id                                                  as competition_id,
       p.id                                                                 as place_id,
       case
           when stage_name = 'Quarter-final' then 18
           when stage_name = 'Semi-final' then 19
           when stage_name = 'Play-off for third place' then 20
           else s.id
           end                                                              as stage_id,
       lmstf.match_status_id                                                as status_id,
       DATE_FORMAT(FROM_UNIXTIME(UnixTimestamp), '%Y-%m-%dT%H:%i:%sZ')      as date,
       DATE_FORMAT(FROM_UNIXTIME(UnixTimestampLocal), '%Y-%m-%dT%H:%i:%sZ') as local_date,
       t_h.id                                                               as home_team_id,
       t_a.id                                                               as away_team_id,
       uu.HomeTeamScore                                                     as goals_home_team,
       uu.AwayTeamScore                                                     as goals_away_team,
       uu.HomeTeamScore                                                     as total_home_goals,
       uu.AwayTeamScore                                                     as total_away_goals,
       uu.HomeTeamPenaltyScore                                              as penalty_shootout_home_goals,
       uu.AwayTeamPenaltyScore                                              as penalty_shootout_away_goals,
       MatchNumber                                                          as game_order
from (select case
                 when StageNameDescription = 'Group Matches' then GroupNameDescription
                 else StageNameDescription
                 end as stage_name,
             uu.*
      from fifa_matches_u20 uu) as uu

         join link_competitions_to_fifa lctf
              on lctf.season_id_fifa = uu.IdSeason
                  and lctf.competition_id_fifa = uu.IdCompetition

         left join teams t_h
                   on t_h.short_name = uu.HomeAbbreviation
         left join teams t_a
                   on t_a.short_name = uu.AwayAbbreviation
         left join stages s
                   on s.title = uu.stage_name
         left join link_match_status_to_fifa lmstf
                   on lmstf.match_status_id_fifa = uu.MatchStatus
         left join places p on uu.StadiumName = p.stadium



set @row_number = (select max(id) from  competitions)
insert into  competitions (id,
                           number_of_match_days,
                           number_of_games,
                           number_of_teams,
                           last_updated,
                           caption,
                           abbreviation,
                           league,
                           year)
select  (@row_number := @row_number + 1) AS id,
        x.number_of_match_days,
        x.number_of_games,
        x. number_of_teams,
        now() as last_updated,
        replace(SeasonName, '™', '') as caption,
        Abbreviation as abbreviation,
        'WC U-20' as league,
        2023 as year
from fifa_competition_seasons s
         join (select IdSeason,
                      count(distinct DATE_FORMAT(FROM_UNIXTIME(UnixTimestamp), '%Y-%m-%d')) as number_of_match_days,
                      count(*) as  number_of_games,
                      count(distinct HomeTeamName) as number_of_teams
               from fifa_matches_u20 uu
               where IdSeason = 284700) as x
              on s.IdSeason = x.IdSeason
where s.IdSeason = 284700


create view matches_u20_staging as
select lctf.competition_id                                                  as competition_id,
       p.id                                                                 as place_id,
       case
           when stage_name = 'Quarter-final' then 18
           when stage_name = 'Semi-final' then 19
           when stage_name = 'Play-off for third place' then 20
           else s.id
           end                                                              as stage_id,
       lmstf.match_status_id                                                as status_id,
       DATE_FORMAT(FROM_UNIXTIME(UnixTimestamp), '%Y-%m-%dT%H:%i:%sZ')      as date,
       DATE_FORMAT(FROM_UNIXTIME(UnixTimestampLocal), '%Y-%m-%dT%H:%i:%sZ') as local_date,
       t_h.id                                                               as home_team_id,
       t_a.id                                                               as away_team_id,
       uu.HomeTeamScore                                                     as goals_home_team,
       uu.AwayTeamScore                                                     as goals_away_team,
       uu.HomeTeamScore                                                     as total_home_goals,
       uu.AwayTeamScore                                                     as total_away_goals,
       uu.HomeTeamPenaltyScore                                              as penalty_shootout_home_goals,
       uu.AwayTeamPenaltyScore                                              as penalty_shootout_away_goals,
       MatchNumber                                                          as game_order
from (select case
                 when StageNameDescription = 'Group Matches' then GroupNameDescription
                 else StageNameDescription
                 end as stage_name,
             uu.*
      from fifa_matches_u20 uu) as uu

         join link_competitions_to_fifa lctf
              on lctf.season_id_fifa = uu.IdSeason
                  and lctf.competition_id_fifa = uu.IdCompetition

         left join teams t_h
                   on t_h.short_name = uu.HomeAbbreviation
         left join teams t_a
                   on t_a.short_name = uu.AwayAbbreviation
         left join stages s
                   on s.title = uu.stage_name
         left join link_match_status_to_fifa lmstf
                   on lmstf.match_status_id_fifa = uu.MatchStatus
         left join places p on uu.StadiumName = p.stadium


# обновление результатов
update matches m
    join matches_u20_staging s
    on m.competition_id = s.competition_id
        and m.game_order = s.game_order

set m.status_id                   = s.status_id,
    m.goals_away_team             = s.goals_away_team,
    m.goals_home_team             = s.goals_home_team,
    m.total_away_goals            = s.total_away_goals,
    m.total_home_goals            = s.total_home_goals,
    m.penalty_shootout_away_goals = s.penalty_shootout_away_goals,
    m.penalty_shootout_home_goals = s.penalty_shootout_home_goals,
    m.home_team_id                = s.home_team_id,
    m.away_team_id                = s.away_team_id


where ((m.status_id != s.status_id)
    or ifnull(m.goals_away_team, 0) != ifnull(s.goals_away_team, 0)
    or ifnull(m.goals_home_team, 0) != ifnull(s.goals_home_team, 0)
    or ifnull(m.total_away_goals, 0) != ifnull(s.total_away_goals, 0)
    or ifnull(m.total_home_goals, 0) != ifnull(s.total_home_goals, 0)
    or ifnull(m.penalty_shootout_away_goals, 0) != ifnull(s.penalty_shootout_away_goals, 0)
    or ifnull(m.penalty_shootout_home_goals, 0) != ifnull(s.penalty_shootout_home_goals, 0)
    or ifnull(m.home_team_id, 0) != ifnull(s.home_team_id, 0)
    or ifnull(m.away_team_id, 0) != ifnull(s.away_team_id, 0))
